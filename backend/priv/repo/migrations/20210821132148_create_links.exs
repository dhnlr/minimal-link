defmodule Minimallink.Repo.Migrations.CreateLinks do
  use Ecto.Migration

  def change do
    create table(:links) do
      add :name, :string
      add :slug, :string
      add :instagram, :string
      add :twitter, :string
      add :youtube, :string
      add :user_id, references(:users, on_delete: :delete_all)

      timestamps()
    end

    create unique_index(:links, [:slug, :user_id])
    create index(:links, [:user_id])
  end
end
