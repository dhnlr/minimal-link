defmodule Minimallink.Repo.Migrations.CreateSites do
  use Ecto.Migration

  def change do
    create table(:sites) do
      add :name, :string
      add :url, :string
      add :priority, :integer
      add :link_id, references(:links, on_delete: :delete_all)

      timestamps()
    end

    create index(:sites, [:link_id])
  end
end
