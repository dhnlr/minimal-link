defmodule Minimallink.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users) do
      add :email, :binary
      add :email_hash, :binary
      add :username, :binary
      add :username_hash, :binary
      add :password_hash, :binary

      timestamps()
    end

    create(unique_index(:users, [:email_hash]))
    create(unique_index(:users, [:username_hash]))
  end
end
