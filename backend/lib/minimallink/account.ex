defmodule Minimallink.Account do
  @moduledoc """
  The Accounts context: public interface for account functionality.
  """

  import Ecto.Query, warn: false
  alias Minimallink.{Repo, PasswordField}

  alias Minimallink.User

  @doc """
  Returns the user with the given `id`.

  Returns `nil` if the user does not exist.
  """
  def get_user(id) do
    Repo.get(User, id)
  end

  @doc """
  Creates a user.
  """
  def create_user(attrs) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  def authenticate(username, password) do
    user = Repo.get_by!(User, username_hash: username)

    with %{password_hash: password_hash} <- user,
      true <- PasswordField.verify_password(password, password_hash) do
      {:ok, user}
    else
      _ -> :error
    end
  end
end
