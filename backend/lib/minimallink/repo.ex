defmodule Minimallink.Repo do
  use Ecto.Repo,
    otp_app: :minimallink,
    adapter: Ecto.Adapters.Postgres
end
