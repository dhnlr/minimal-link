defmodule Minimallink.Content do
  import Ecto.Query, warn: false
  alias Minimallink.Repo

  alias Minimallink.{User, Link, Site}

  def create_link(%User{} = user, attrs) do
    %Link{}
    |> Link.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:user, user)
    |> Repo.insert()
  end

  def create_site(%Link{} = link, attrs) do
    %Site{}
    |> Site.changeset(attrs)
    |> Ecto.Changeset.put_assoc(:link, link)
    |> Repo.insert()
  end

  def get_link_by_slug(slug) do
    Repo.get_by(Link, slug: slug)
  end

  def get_link_by_user(id) do
    Repo.get_by(Link, user_id: id)
  end

  def get_link_by_id(id) do
    Repo.get_by(Link, id: id)
    # |> Repo.preload(sites: :link)
  end

  def get_link_by_id_user_id(id, user_id) do
    Repo.get_by(Link, %{id: id, user_id: user_id})
  end

  def get_site_by_link(id) do
    Repo.all(from s in Site, where: s.link_id == ^id)
  end

  def update_link(%Link{} = link, attrs) do
    link
    |> Link.changeset(attrs)
    |> Repo.update
  end

  def update_link(nil, _) do
    :error
  end

  def update_site(%Site{} = site, attrs) do
    site
    |> Site.changeset(attrs)
    |> Repo.update()
  end

  def update_site(nil, _) do
    :error
  end

  def delete_site(id) do
    Repo.get(Site, id)
    |> Repo.delete()
  end
end
