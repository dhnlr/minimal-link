defmodule Minimallink.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias Minimallink.{User, Repo, HashField, PasswordField, EncryptedField}

  schema "users" do
    field :email, EncryptedField
    field :email_hash, HashField
    field :password, :binary, virtual: true
    field :password_hash, PasswordField
    field :username, EncryptedField
    field :username_hash, HashField

    has_many :links, Minimallink.Link

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    required_fields = [:email, :username, :password]
    optional_fields = [:email_hash, :username_hash, :password_hash]
    user
    |> cast(attrs, required_fields ++ optional_fields)
    |> validate_required(required_fields)
    |> validate_length(:password, min: 6)
    |> validate_format(:password, ~r/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{6,}$/)
    |> validate_format(:email, ~r/([\w\.\-_]+)?\w+@[\w-_]+(\.\w+){1,}/i)
    |> validate_format(:username, ~r/^[\w]*$/)
    |> add_email_hash()
    |> add_username_hash()
    |> add_password_hash()
    |> unique_constraint(:username_hash, name: :users_username_hash_index)
    |> unique_constraint(:email_hash, name: :users_email_hash_index)
  end

  @doc """
  Retrieve one user from the database by email address
  """
  def get_by_email(email) do
    Repo.get_by(User, email_hash: email)
  end

  defp add_email_hash(changeset) do
    if Map.has_key?(changeset.changes, :email) do
      changeset |> put_change(:email_hash, changeset.changes.email)
    else
      changeset
    end
  end

  defp add_username_hash(changeset) do
    if Map.has_key?(changeset.changes, :username) do
      changeset |> put_change(:username_hash, changeset.changes.username)
    else
      changeset
    end
  end

  defp add_password_hash(changeset) do
    if Map.has_key?(changeset.changes, :password) do
      changeset |> put_change(:password_hash, changeset.changes.password)
    else
      changeset
    end
  end
end
