defmodule Minimallink.Link do
  use Ecto.Schema
  import Ecto.Changeset

  schema "links" do
    field :name, :string
    field :slug, :string
    field :instagram, :string
    field :twitter, :string
    field :youtube, :string

    belongs_to :user, Minimallink.User
    has_many :sites, Minimallink.Site

    timestamps()
  end

  @doc false
  def changeset(link, attrs) do
    required_fields = [:name, :slug]
    optional_fields = [:instagram, :twitter, :youtube]

    link
    |> cast(attrs, required_fields ++ optional_fields)
    |> validate_required(required_fields)
    |> unique_constraint(:slug, name: :links_slug_user_id_index)
    |> unique_constraint(:user_id, name: :links_slug_user_id_index)
    |> assoc_constraint(:user)
  end
end
