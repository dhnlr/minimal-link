defmodule Minimallink.Site do
  use Ecto.Schema
  import Ecto.Changeset

  schema "sites" do
    field :name, :string
    field :priority, :integer
    field :url, :string

    belongs_to :link, Minimallink.Link

    timestamps()
  end

  @doc false
  def changeset(site, attrs) do
    required_field = [:name, :url]
    additonal_field = [:priority]
    site
    |> cast(attrs, required_field ++ additonal_field)
    |> validate_required(required_field)
    |> assoc_constraint(:link)
  end
end
