defmodule MinimallinkWeb.Schema do
  use Absinthe.Schema
  alias MinimallinkWeb.Resolvers.{ContentResolver, UserResolver}
  alias MinimallinkWeb.Middlewares

  import_types(MinimallinkWeb.Types.ContentTypes)
  import_types(MinimallinkWeb.Types.UserTypes)

  query do
    @desc "Get a content by its slug"
    field :content, :content do
      arg(:slug, :string)
      arg(:user_id, :id)
      resolve(&ContentResolver.content/3)
    end

    @desc "Get content by active user"
    field :mine, :content do
      middleware Middlewares.Authentication
      resolve(&ContentResolver.mine/3)
    end

    # @desc "Get sites by its link"
    # field :site, :site do
    #   arg(:link_id, :id)
    #   # resolve(&)
    # end
  end

  mutation do
    @desc "Create a new user"
    field :create_user, :user do
      arg :username, non_null(:string)
      arg :email, non_null(:string)
      arg :password, non_null(:string)
      resolve(&UserResolver.register/3)
    end

    @desc "Sign-in a user"
    field :signin, :session do
     arg :username, non_null(:string)
     arg :password, non_null(:string)
     resolve(&UserResolver.login/3)
     middleware Middlewares.Token
    end

    @desc "Create a new content"
    field :create_content, :content do
      arg :name, non_null(:string)
      arg :slug, non_null(:string)
      arg :instagram, non_null(:string)
      arg :twitter, non_null(:string)
      arg :youtube, non_null(:string)
      middleware Middlewares.Authentication
      resolve(&ContentResolver.create_content/3)
    end

    @desc "Update a content"
    field :update_content, :content do
      arg :id, non_null(:id)
      arg :name, non_null(:string)
      arg :slug, non_null(:string)
      arg :instagram, non_null(:string)
      arg :twitter, non_null(:string)
      arg :youtube, non_null(:string)
      middleware Middlewares.Authentication
      resolve(&ContentResolver.update_content/3)
    end

    @desc "Create a site"
    field :create_site, :site do
      arg :name, non_null(:string)
      arg :url, non_null(:string)
      arg :priority, :integer
      arg :link_id, non_null(:id)
      middleware Middlewares.Authentication
      resolve(&ContentResolver.create_site/3)
    end

    @desc "Update a site"
    field :update_site, :site do
      arg :id, non_null(:id)
      arg :link_id, non_null(:id)
      arg :name, non_null(:string)
      arg :url, non_null(:string)
      arg :priority, :integer
      middleware Middlewares.Authentication
      resolve(&ContentResolver.update_site/3)
    end

    @desc "Delete a site"
    field :delete_site, :site do
      arg :id, non_null(:id)
      middleware Middlewares.Authentication
      resolve(&ContentResolver.delete_site/3)
    end
  end

  def context(ctx) do
    source = Dataloader.Ecto.new(Minimallink.Repo)

    loader =
      Dataloader.new()
      |> Dataloader.add_source(Minimallink.Content, source)

    Map.put(ctx, :loader, loader)
  end

  def plugins do
    [Absinthe.Middleware.Dataloader] ++ Absinthe.Plugin.defaults()
  end
end
