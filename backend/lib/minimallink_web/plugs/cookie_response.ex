defmodule MinimallinkWeb.Plugs.CookieResponse do
  @doc """
  Implemnent from https://elixirforum.com/t/absinthe-authentication-with-jwt-tokens/39551/15
  """
  def absinthe_before_send(conn, %Absinthe.Blueprint{} = blueprint) do
    if auth_token = blueprint.execution.context[:auth_token] do

      Plug.Conn.put_resp_cookie(
        conn,
        # This name matches what is expected by Guardian.Plug.VerifyCookie
        "token",
        auth_token || "",
        # Setting expires in the past is the official way to delete a cookie
        # https://stackoverflow.com/a/53573622
        max_age: if(auth_token, do: 25 * 365 * 24 * 60 * 60, else: -100_000),
        http_only: true,
        secure: false
      )
    else
      conn
    end
  end

  def absinthe_before_send(conn, _) do
    conn
  end
end
