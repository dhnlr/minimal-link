defmodule MinimallinkWeb.Middlewares.Authentication do
  @behaviour Absinthe.Middleware

  def call(resolution, _) do
    case resolution.context do
      %{current_user: _} ->
        resolution
      _ ->
        resolution
        |> Absinthe.Resolution.put_result({:error, "Please login first"})
    end
  end
end
