defmodule MinimallinkWeb.AuthToken do
  @dialyzer {:no_return, {:sign, 1}}
  @dialyzer {:no_return, {:verify, 1}}

  def sign(id) do
    Paseto.V2.sign(id, Application.get_env(:minimallink, MinimallinkWeb.AuthToken)[:sk])
  end

  def verify(token) do
    token = token |> String.split(".") |> Enum.at(2)
    Paseto.V2.verify(token, Application.get_env(:minimallink, MinimallinkWeb.AuthToken)[:pk])
  end
end
