defmodule MinimallinkWeb.Types.ContentTypes do
  use Absinthe.Schema.Notation

  import_types Absinthe.Type.Custom
  import Absinthe.Resolution.Helpers, only: [dataloader: 3]

  object :content do
    field :id, non_null(:id)
    field :name, non_null(:string)
    field :slug, non_null(:string)
    field :instagram, non_null(:string)
    field :twitter, non_null(:string)
    field :youtube, non_null(:string)
    field :inserted_at, :naive_datetime
    field :users, non_null(:user), resolve: dataloader(Minimallink.Content, :user, args: %{})
    field :sites, non_null(list_of(:site)), resolve: dataloader(Minimallink.Content, :sites, args: %{})
  end

  object :site do
    field :id, non_null(:id)
    field :name, non_null(:string)
    field :url, non_null(:string)
    field :priority, :integer
    field :contents, non_null(:content), resolve: dataloader(Minimallink.Content, :content, args: %{})
  end
end
