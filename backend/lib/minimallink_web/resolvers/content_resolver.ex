defmodule MinimallinkWeb.Resolvers.ContentResolver do
  alias Minimallink.Content
  alias MinimallinkWeb.ChangesetErrors

  def content(_, %{slug: slug}, _) do
    {:ok, Content.get_link_by_slug(slug)}
  end

  def mine(_, _, %{context: %{current_user: user}}) do
    {:ok, Content.get_link_by_user(user.id)}
  end

  def create_content(_, args, %{context: %{current_user: user}}) do
    case Content.create_link(user, args) do
      {:error, changeset} ->
        {
          :error,
          message: "Could not create list!",
          details: ChangesetErrors.errro_details(changeset)
        }
      {:ok, contents} ->
        {:ok, contents}
    end
  end

  def create_site(_, args, _) do
    list = Content.get_link_by_id(args.link_id)

    case Content.create_site(list, args) do
      {:error, changeset} ->
        {
          :error,
          message: "Could not create list!",
          details: ChangesetErrors.errro_details(changeset)
        }
      {:ok, site} ->
        {:ok, site}
    end
  end

  def update_content(_, args, %{context: %{current_user: user}}) do
    list = Content.get_link_by_id_user_id(args.id, user.id)

    case Content.update_link(list, args) do
      {:ok, list} ->
        {:ok, list}
      {:error, changeset} ->
        {
          :error,
          message: "Could not update list!",
          details: ChangesetErrors.errro_details(changeset)
        }
      _ ->
        {
          :error,
          message: "Could not update list!",
          details: "List not found"
        }
    end
  end

  def update_site(_, args, _) do
    site = Content.get_site_by_link(args.link_id)

    case Content.update_site(site, args) do
      {:ok, site} ->
        {:ok, site}
        {:error, changeset} ->
          {
            :error,
            message: "Could not update site!",
            details: ChangesetErrors.errro_details(changeset)
          }
        _ ->
          {
            :error,
            message: "Could not update site!",
            details: "Site not found"
          }
    end
  end

  def delete_site(_, %{id: id}, _) do
    case Content.delete_site(id) do
      {:ok, site} ->
        {:ok, site}
      {:error, changeset} ->
        {
          :error,
          message: "Could not delete site!",
          details: ChangesetErrors.errro_details(changeset)
        }
      _ ->
        {
          :error,
          message: "Could not delete site!",
          details: "Site not found"
        }
    end
  end
end
