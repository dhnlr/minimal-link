defmodule MinimallinkWeb.Resolvers.UserResolver do
  alias Minimallink.Account
  alias MinimallinkWeb.ChangesetErrors

  def register(_, args, _) do
    case Account.create_user(args) do
      {:ok, user} ->
        {:ok, user}
      {:error, changeset} ->
        {
          :error,
          message: "Could not create account",
          details: ChangesetErrors.errro_details(changeset)
        }
    end
  end

  def login(_, %{username: username, password: password}, _) do
    case Account.authenticate(username, password) do
      {:ok, user} ->
        token = MinimallinkWeb.AuthToken.sign(to_string(user.id))
        {:ok, %{user: user, token: token}}
      _ ->
        {
          :error,
          message: "Could not login",
          details: "Make sure username or password not wrong"
        }
    end
  end
end
