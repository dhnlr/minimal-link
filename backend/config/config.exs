# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :minimallink,
  ecto_repos: [Minimallink.Repo]

# Configures the endpoint
config :minimallink, MinimallinkWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Zdxv4KvEFK9LQUjzX1+hkoCspUD3kTloQB6pcp7CX4OZfVfIsn3z7dNnsrUkDAYy",
  render_errors: [view: MinimallinkWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Minimallink.PubSub,
  live_view: [signing_salt: "ck4qg5Sk"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Set the Encryption Keys as an "Application Variable" accessible in aes.ex
config :minimallink, Minimallink.AES,
  # get the ENCRYPTION_KEYS env variable
  keys:
    "a7kOCXUIorl5ka6PATZs+TiAchwvXl3PeRWO2TNKMnY="
    # remove single-quotes around key list in .env
    |> String.replace("'", "")
    # split the CSV list of keys
    |> String.split(",")
    # decode the key.
    |> Enum.map(fn key -> :base64.decode(key) end)

config :minimallink, MinimallinkWeb.AuthToken,
  pk: "DcLZHOGSPXupgt7FXWBCd73oe60GotytDyY4jvDGf2w=" |> Base.decode64!(),
  sk:
    "eZ3t0AH1bOOyqY9u5U3u9asrTxqQn2I2q/3qTWzAoBUNwtkc4ZI9e6mC3sVdYEJ3veh7rQai3K0PJjiO8MZ/bA=="
    |> Base.decode64!()

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
