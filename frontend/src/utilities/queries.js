import { gql } from "@apollo/client";

export const LOGIN = gql`
  mutation Login($username: String!, $password: String!) {
    signin(username: $username, password: $password) {
      token
      user {
        id
      }
    }
  }
`;

export const SLUG = gql`
  query ($slug: String!) {
    content(slug: $slug) {
      id
      name
      instagram
      youtube
      twitter
      sites {
        id
        name
        url
      }
    }
  }
`;

export const MINE = gql`
  query {
    mine {
      id
      name
      slug
      instagram
      youtube
      twitter
      sites {
        id
        name
        url
        priority
      }
    }
  }
`;

export const UPDATE_LINK = gql`
  mutation UpdateContent(
    $id: ID!
    $name: String!
    $slug: String!
    $instagram: String!
    $twitter: String!
    $youtube: String!
  ) {
    updateContent(
      id: $id
      name: $name
      slug: $slug
      instagram: $instagram
      twitter: $twitter
      youtube: $youtube
    ) {
      id
    }
  }
`;

export const INSERT_LINK = gql`
  mutation InsertContent(
    $name: String!
    $slug: String!
    $instagram: String!
    $twitter: String!
    $youtube: String!
  ) {
    createContent(
      name: $name
      slug: $slug
      instagram: $instagram
      twitter: $twitter
      youtube: $youtube
    ) {
      id
    }
  }
`;

export const INSERT_SITE = gql`
  mutation CreateSite($name: String!, $url: String!, $link_id: ID!) {
    createSite(name: $name, url: $url, link_id: $link_id) {
      id
    }
  }
`;

export const UPDATE_SITE = gql`
  mutation UpdateSite($id: ID!, $name: String!, $url: String!, $link_id: ID!) {
    updateSite(id: $id, name: $name, url: $url, link_id: $link_id) {
      id
    }
  }
`;

export const DELETE_SITE = gql`
  mutation DeleteSite($id: ID!) {
    deleteSite(id: $id) {
      id
    }
  }
`;
